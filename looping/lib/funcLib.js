//Soal 4
function perulanganWhile() {
    i = 2;
    while(i<=20){
        console.log(i+" - I love coding");
        i=i+2;
    }
    i = i-2;
    while(i>=2){
        console.log(i+" - I will become a mobile developer");
        i=i-2;
    }
}

//Soal 5
function perulanganFor() {
    for (let i = 1; i <= 20; i++) {
        if(i%3 == 0 && i%2 != 0){
            console.log(i+" - I Love Conding")
        }else if(i%2 != 0){
            console.log(i+" - Santai")
        }else if(i%2 == 0){
            console.log(i+" - Berkualitas")
        }
    }
}

function persegiPanjang(panjang,lebar) {
    for (let i = 0; i < lebar; i++) {
        for (let j = 0; j < panjang; j++) {
           process.stdout.write("#");
        }
        console.log("")
    }
}

function tangga(sisi) {
    for (let i = 0; i <= sisi; i++) {
        for (let j = 0; j < i; j++) {
           process.stdout.write("#");
        }
        console.log("")
    }
}

function catur(sisi) {
    for (let i = 0; i <= sisi; i++) {
        if(i%2==0){
            for (let j = 0; j <= sisi; j++) {
                if(j%2==0){
                    process.stdout.write(" ");
                }else{
                    process.stdout.write("#");
                }
                
            }
        }else{
            for (let j = 0; j < sisi; j++) {
                if(j%2==0){
                    process.stdout.write("#");
                }else{
                    process.stdout.write(" ");
                }
                
            }
        }
        console.log("")
    }
}

module.exports = {
    perulanganWhile:perulanganWhile,
    perulanganFor:perulanganFor,
    persegiPanjang:persegiPanjang,
    tangga:tangga,
    catur:catur,
}