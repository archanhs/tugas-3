var funcLib = require('./lib/funcLib');

var perulanganWhile = funcLib.perulanganWhile;
var perulanganFor = funcLib.perulanganFor;
var persegiPanjang = funcLib.persegiPanjang;
var tangga = funcLib.tangga;
var catur = funcLib.catur;

var args = process.argv
var perintah = args[2];
switch (perintah){
    case "while":
        perulanganWhile()
        break;
    case "for":
        perulanganFor()
        break;
    case "persegiPanjang":
        persegiPanjang(args[3],args[4])
        break;
    case "tangga":
        tangga(args[3])
        break;
    case "catur":
        catur(args[3])
        break;
    default:
        console.log("perintah tidak terima")
        break;
}

