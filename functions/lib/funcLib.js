//Soal 1
function teriak(){
    return "Halo Sanbers!";
}
//Soal 2
function kalikan(num1,num2){
    return num1*num2;
}
//Soal 3
function introduce(nama,age,hobby){
    return "Nama saya "+nama+", umur saya "+age+" tahun, dan saya punya hobby yaitu "+hobby+"!";
}
module.exports = {
    teriak:teriak,
    kalikan:kalikan,
    kenalan:introduce,
}
