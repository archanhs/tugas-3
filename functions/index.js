var funcLib = require('./lib/funcLib');

var teriak = funcLib.teriak;
var kalikan = funcLib.kalikan;
var kenalan = funcLib.kenalan;

var args = process.argv
var perintah = args[2];
switch (perintah){
    case "teriak":
        console.log(teriak())
        break;
    case "kalikan":
        console.log(kalikan(args[3],args[4]))
        break;
    case "kenalan":
        console.log(kenalan(args[3],args[4],args[5]))
        break;
    default:
        console.log("perintah tidak terima")
        break;
}

// //Soal 4
// i = 2;
// while(i<=20){
//     console.log(i+" - I love coding");
//     i=i+2;
// }
// i = i-2;
// while(i>=2){
//     console.log(i+" - I will become a mobile developer");
//     i=i-2;
// }
// //Soal 5
// for (let i = 1; i <= 20; i++) {
//     if(i%3 == 0 && i%2 != 0){
//         console.log(i+" - I Love Conding")
//     }else if(i%2 != 0){
//         console.log(i+" - Santai")
//     }else if(i%2 == 0){
//         console.log(i+" - Berkualitas")
//     }
// }
// //Soal 6
// for (let i = 1; i <= 20; i++) {
//     if(i%3 == 0 && i%2 != 0){
//         console.log(i+" - I Love Conding")
//     }else if(i%2 != 0){
//         console.log(i+" - Santai")
//     }else if(i%2 == 0){
//         console.log(i+" - Berkualitas")
//     }
// }